

function * Fibonacci {

	yield 0
	yield 1

	let a = 0;
	let b = 1;

	while(true){
		yield a + b;
		[a , b] = [b, a + b]
	}

}

let generador = Fibonacci();

console.log(generador.next());
console.log(generador.next());
console.log(generador.next());